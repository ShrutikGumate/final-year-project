package com.example.addtwonum;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText num1;
    private EditText num2;
    private Button add;
    private Button sub;
    private TextView result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num1=(EditText)findViewById(R.id.etN1);
        num2=(EditText)findViewById(R.id.etN2);
        add=(Button)findViewById(R.id.addbtn);
        sub=(Button)findViewById(R.id.subbtn);
        result=(TextView)findViewById(R.id.textResult);




        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n1=Integer.parseInt(num1.getText().toString());
                int n2=Integer.parseInt(num2.getText().toString());
                int subtarct = n1-n2;
                result.setText("Answer : "+subtarct);

            }
        });
    }
    public void onclickadd(View v){
        num1=(EditText)findViewById(R.id.etN1);
        num2=(EditText)findViewById(R.id.etN2);
        add=(Button)findViewById(R.id.addbtn);
        sub=(Button)findViewById(R.id.subbtn);
        result=(TextView)findViewById(R.id.textResult);
        int n1,n2;
        String s1=num1.getText().toString();
        if (s1.length()==0){
            n1=0;
        }
        else{
            n1=Integer.parseInt(s1);
        }
        String s2=num2.getText().toString();
        if (s2.length()==0){
            n2=0;
        }
        else{
            n2=Integer.parseInt(s2);
        }
        int sum = n1+n2;
        Toast.makeText(this,String.valueOf(sum), Toast.LENGTH_SHORT).show();

    }
}
